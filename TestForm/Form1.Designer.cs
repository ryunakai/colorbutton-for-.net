﻿namespace TestForm
{
    partial class Form1
    {
        /// <summary>
        /// 必要なデザイナー変数です。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 使用中のリソースをすべてクリーンアップします。
        /// </summary>
        /// <param name="disposing">マネージ リソースが破棄される場合 true、破棄されない場合は false です。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows フォーム デザイナーで生成されたコード

        /// <summary>
        /// デザイナー サポートに必要なメソッドです。このメソッドの内容を
        /// コード エディターで変更しないでください。
        /// </summary>
        private void InitializeComponent()
        {
            this.colorButton8 = new Pluslus.Forms.Controls.ColorButton();
            this.colorButton7 = new Pluslus.Forms.Controls.ColorButton();
            this.colorButton6 = new Pluslus.Forms.Controls.ColorButton();
            this.colorButton4 = new Pluslus.Forms.Controls.ColorButton();
            this.colorButton5 = new Pluslus.Forms.Controls.ColorButton();
            this.colorButton3 = new Pluslus.Forms.Controls.ColorButton();
            this.colorButton2 = new Pluslus.Forms.Controls.ColorButton();
            this.colorButton1 = new Pluslus.Forms.Controls.ColorButton();
            this.SuspendLayout();
            // 
            // colorButton8
            // 
            this.colorButton8.BackColor = System.Drawing.Color.DarkRed;
            this.colorButton8.DisabledColor = System.Drawing.Color.Gray;
            this.colorButton8.ForeColor = System.Drawing.SystemColors.ButtonShadow;
            this.colorButton8.Location = new System.Drawing.Point(315, 211);
            this.colorButton8.Name = "colorButton8";
            this.colorButton8.Size = new System.Drawing.Size(148, 48);
            this.colorButton8.TabIndex = 3;
            this.colorButton8.Text = "colorButton8";
            this.colorButton8.UseVisualStyleBackColor = false;
            this.colorButton8.Click += new System.EventHandler(this.colorButton8_Click);
            // 
            // colorButton7
            // 
            this.colorButton7.Location = new System.Drawing.Point(79, 283);
            this.colorButton7.Name = "colorButton7";
            this.colorButton7.Size = new System.Drawing.Size(170, 58);
            this.colorButton7.TabIndex = 2;
            this.colorButton7.Text = "colorButton7";
            this.colorButton7.UseVisualStyleBackColor = true;
            // 
            // colorButton6
            // 
            this.colorButton6.BackColor = System.Drawing.Color.DarkRed;
            this.colorButton6.Enabled = false;
            this.colorButton6.Location = new System.Drawing.Point(249, 120);
            this.colorButton6.Name = "colorButton6";
            this.colorButton6.Size = new System.Drawing.Size(161, 55);
            this.colorButton6.TabIndex = 1;
            this.colorButton6.Text = "colorButton6";
            this.colorButton6.UseVisualStyleBackColor = false;
            // 
            // colorButton4
            // 
            this.colorButton4.BackColor = System.Drawing.Color.MediumVioletRed;
            this.colorButton4.CornerRadius = 10;
            this.colorButton4.DisabledColor = System.Drawing.Color.DimGray;
            this.colorButton4.Enabled = false;
            this.colorButton4.Font = new System.Drawing.Font("メイリオ", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.colorButton4.ForeColor = System.Drawing.Color.PaleGoldenrod;
            this.colorButton4.Location = new System.Drawing.Point(305, 303);
            this.colorButton4.Name = "colorButton4";
            this.colorButton4.Size = new System.Drawing.Size(133, 39);
            this.colorButton4.TabIndex = 0;
            this.colorButton4.Text = "ボタン(&B)";
            this.colorButton4.UseVisualStyleBackColor = false;
            // 
            // colorButton5
            // 
            this.colorButton5.BackColor = System.Drawing.Color.Peru;
            this.colorButton5.CornerRadius = 200;
            this.colorButton5.Font = new System.Drawing.Font("メイリオ", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.colorButton5.ForeColor = System.Drawing.Color.Black;
            this.colorButton5.Location = new System.Drawing.Point(315, 12);
            this.colorButton5.Name = "colorButton5";
            this.colorButton5.Size = new System.Drawing.Size(155, 59);
            this.colorButton5.TabIndex = 0;
            this.colorButton5.Text = "ボタン(&B)";
            this.colorButton5.UseVisualStyleBackColor = false;
            this.colorButton5.Click += new System.EventHandler(this.colorButton5_Click_1);
            // 
            // colorButton3
            // 
            this.colorButton3.BackColor = System.Drawing.Color.DodgerBlue;
            this.colorButton3.CornerRadius = 10;
            this.colorButton3.Font = new System.Drawing.Font("メイリオ", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.colorButton3.ForeColor = System.Drawing.Color.Black;
            this.colorButton3.Location = new System.Drawing.Point(12, 137);
            this.colorButton3.Name = "colorButton3";
            this.colorButton3.Size = new System.Drawing.Size(122, 39);
            this.colorButton3.TabIndex = 0;
            this.colorButton3.Text = "ボタン(&B)";
            this.colorButton3.UseVisualStyleBackColor = false;
            // 
            // colorButton2
            // 
            this.colorButton2.BackColor = System.Drawing.Color.ForestGreen;
            this.colorButton2.CornerRadius = 10;
            this.colorButton2.Font = new System.Drawing.Font("メイリオ", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.colorButton2.ForeColor = System.Drawing.Color.PaleGoldenrod;
            this.colorButton2.Location = new System.Drawing.Point(12, 86);
            this.colorButton2.Name = "colorButton2";
            this.colorButton2.Size = new System.Drawing.Size(149, 35);
            this.colorButton2.TabIndex = 0;
            this.colorButton2.Text = "ボタン(&B)";
            this.colorButton2.UseVisualStyleBackColor = false;
            // 
            // colorButton1
            // 
            this.colorButton1.BackColor = System.Drawing.Color.MidnightBlue;
            this.colorButton1.CornerRadius = 10;
            this.colorButton1.Font = new System.Drawing.Font("メイリオ", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.colorButton1.ForeColor = System.Drawing.Color.PaleGoldenrod;
            this.colorButton1.Location = new System.Drawing.Point(61, 201);
            this.colorButton1.Name = "colorButton1";
            this.colorButton1.Size = new System.Drawing.Size(234, 59);
            this.colorButton1.TabIndex = 0;
            this.colorButton1.Text = "ボタン(&B)";
            this.colorButton1.UseVisualStyleBackColor = false;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.WhiteSmoke;
            this.ClientSize = new System.Drawing.Size(482, 354);
            this.Controls.Add(this.colorButton8);
            this.Controls.Add(this.colorButton7);
            this.Controls.Add(this.colorButton6);
            this.Controls.Add(this.colorButton4);
            this.Controls.Add(this.colorButton5);
            this.Controls.Add(this.colorButton3);
            this.Controls.Add(this.colorButton2);
            this.Controls.Add(this.colorButton1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);

        }

        #endregion

        private Pluslus.Forms.Controls.ColorButton colorButton1;
        private Pluslus.Forms.Controls.ColorButton colorButton2;
        private Pluslus.Forms.Controls.ColorButton colorButton3;
        private Pluslus.Forms.Controls.ColorButton colorButton4;
        private Pluslus.Forms.Controls.ColorButton colorButton5;
        private Pluslus.Forms.Controls.ColorButton colorButton6;
        private Pluslus.Forms.Controls.ColorButton colorButton7;
        private Pluslus.Forms.Controls.ColorButton colorButton8;








    }
}

