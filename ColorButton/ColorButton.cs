﻿//ColorButton for .NET
//Copyright(c) 2014 Pluslus ( http://www.pluslus.com/ )
//This software is released under the MIT License, see LICENSE.txt.

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using System.Drawing.Drawing2D;

namespace Pluslus.Forms.Controls
{
    /// <summary>
    /// Color Button Control for Windows Forms
    /// </summary>
    [System.Drawing.ToolboxBitmap(typeof(System.Windows.Forms.Button))]
    public partial class ColorButton : System.Windows.Forms.Button
    {
        /// <summary>
        /// ボタン表面イメージ
        /// Button face image
        /// </summary>
        protected Image m_paintedFace;

        /// <summary>
        /// ボタン表面イメージ（クリック時）
        /// Button face image on clicking
        /// </summary>
        protected Image m_clickedFace;

        /// <summary>
        /// マウスオーバー時に表示されるイメージ
        /// Image shown on mouseover
        /// </summary>
        protected Image m_mouseOverFace;

        /// <summary>
        /// フォーカスがある場合のイメージ
        /// Image shown in having a focus
        /// </summary>
        protected Image m_focusedFace;

        /// <summary>
        /// マウスダウン時 true
        /// Flag for mousedown
        /// </summary>
        protected bool m_mouseDown;

        /// <summary>
        /// スペースキーが押されていれば true
        /// Flag for pressing specekey
        /// </summary>
        protected bool m_spaceKeyPress;

        /// <summary>
        /// マウスオーバー時 true
        /// Flag for mouseover
        /// </summary>
        protected bool m_onMouse;

        /// <summary>
        /// フォーカスを持っている場合true
        /// Flag for a focus
        /// </summary>
        protected bool m_hasFocus;
                

        /// <summary>
        /// オブジェクトを構築します。
        /// Constractor
        /// </summary>
        public ColorButton()
        {
            InitializeComponent();

            this.DoubleBuffered = true; //Use double buffer for painting
            this.ResizeRedraw = true;   //Redraw when this control is resized

            //プロパティの初期化
            //initialize properties
            this.CornerRadius = 6;
            this.MatColor = Color.Empty;
            this.DisabledColor = Color.DarkGray;
        }       

        /// <summary>
        /// 使用中のリソースをすべてクリーンアップします。
        /// clean up resources
        /// </summary>
        /// <param name="disposing">マネージ リソースが破棄される場合 true、破棄されない場合は false です。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }

            if (disposing)
            {
                ClearCachedImage();

                if (this.Region != null)
                {
                    this.Region.Dispose();
                }
            }

            base.Dispose(disposing);
        }

        /// <summary>
        /// field for CornerRadius property
        /// </summary>
        protected int _CornerRadius;
        
        /// <summary>
        /// 角の丸みのための半径を取得または設定します。
        /// Set or get corner radius size
        /// </summary>
        [DefaultValue(6)]
        [Description("角の丸みのための半径を取得または設定します。Set or get corner radius size.")]
        [Category("表示")]
        public virtual int CornerRadius
        {
            set
            {
                _CornerRadius = value;
                this.Invalidate();
            }
            get
            {
                return _CornerRadius;
            }
        }

        /// <summary>
        /// field for MatColor property
        /// </summary>
        protected Color _MatColor;

        /// <summary>
        /// 下地の色を取得または設定します。未設定（Color.Empty）のときは、コントロールのParent.BackColorが使用されます。
        /// Set or get mat color. when this property is not setted, Parent.BackColor will be used.
        /// </summary>
        [DefaultValue(typeof(Color), "")]
        [Description("下地の色を取得または設定します。未設定（Color.Empty）のときは、コントロールのParent.BackColorが使用されます。" +
                     "Set or get mat color. when this property is not setted, Parent.BackColor will be used.")]
        [Category("表示")]
        public virtual Color MatColor
        {
            set
            {
                 _MatColor = value;
                 this.Invalidate();
            }

            get
            {
                return _MatColor;
            }
        }
        /// <summary>
        /// field for DisabledColor
        /// </summary>
        protected Color _DispabledColor;

        /// <summary>
        /// Set or get face color when button is disabled
        /// </summary>
        [DefaultValue(typeof(Color), "DarkGray")]
        [Description("無効時の色を取得または設定します。" +
                     "Set or get face color when button is disabled.")]
        [Category("表示")]
        public virtual Color DisabledColor
        {
            set
            {
                _DispabledColor = value;
                this.Invalidate();
            }
            get
            {
                return _DispabledColor;
            }
        }

        /// <summary>
        /// Override OnEnabledChanged
        /// </summary>
        /// <param name="e"></param>
        protected override void OnEnabledChanged(EventArgs e)
        {
            base.OnEnabledChanged(e);

            if (m_paintedFace != null)
            {
                m_paintedFace.Dispose();
                m_paintedFace = null;
            }
            this.Invalidate();
        }

        /// <summary>
        /// コントロールの描画(オーバーライド)
        /// Paint a control face (override)
        /// </summary>
        /// <param name="pevent"></param>
        protected override void OnPaint(PaintEventArgs pevent)
        {
            //base.OnPaint(pevent);

            if (m_paintedFace == null)  
            {
                //描画用イメージがない場合 when images for drawing a face are not prepared
                
                //リージョンをセット
                //set region
                var path = CreateControlShape(0, 0);

                if (this.Region != null)
                {
                    this.Region.Dispose();
                }

                this.Region = new System.Drawing.Region(path);
                path.Dispose();

                //コントロール描画に使用するイメージを作成します。（インスタンス作成後の初回や、リサイズ、プロパティ変更時）
                //create a face image(bitmap) when this instance is created or this control is resized or property is changed.
                m_paintedFace = CreateFaceImage(false);
                m_clickedFace = CreateFaceImage(true);
                m_mouseOverFace = CreateMouseOverImage();
                m_focusedFace = CreateFocusedImage();
            }

            //作成済みのイメージでコントロール表面を描画します。
            //draw the face of this control with an image on memory created before
            if (m_mouseDown || m_spaceKeyPress)
            {
                pevent.Graphics.DrawImage(m_clickedFace, new Point(0, 0));
            }
            else
            {
                pevent.Graphics.DrawImage(m_paintedFace, new Point(0, 0));
            }

            //マウスオーバーイメージを描画 draw a mouse over image
            if (m_onMouse)
            {
                pevent.Graphics.DrawImage(m_mouseOverFace, new Point(0, 0));
            }

            if (m_hasFocus)
            {
                pevent.Graphics.DrawImage(m_focusedFace, new Point(0, 0));
            }
        }

        /// <summary>
        /// override OnMouseDown
        /// </summary>
        /// <param name="mevent"></param>
        protected override void OnMouseDown(MouseEventArgs mevent)
        {
            m_mouseDown = true;
            base.OnMouseDown(mevent);
        }

        /// <summary>
        /// override OnMouseUp
        /// </summary>
        /// <param name="mevent"></param>
        protected override void OnMouseUp(MouseEventArgs mevent)
        {
            m_mouseDown = false;
            base.OnMouseUp(mevent);
        }

        /// <summary>
        /// override OnKeyDown
        /// </summary>
        /// <param name="kevent"></param>
        protected override void OnKeyDown(KeyEventArgs kevent)
        {
            if (kevent.KeyCode == Keys.Space)
            {
                m_spaceKeyPress = true;
            }
            base.OnKeyDown(kevent);
        }

        /// <summary>
        /// override OnKeyUp
        /// </summary>
        /// <param name="kevent"></param>
        protected override void OnKeyUp(KeyEventArgs kevent)
        {
            if (kevent.KeyCode == Keys.Space)
            {
                m_spaceKeyPress = false;
            }
            base.OnKeyUp(kevent);
        }

        /// <summary>
        /// override OnMouseEnter
        /// </summary>
        /// <param name="e"></param>
        protected override void OnMouseEnter(EventArgs e)
        {
            m_onMouse = true;
            base.OnMouseEnter(e);
        }

        /// <summary>
        /// override MouseLeave
        /// </summary>
        /// <param name="e"></param>
        protected override void OnMouseLeave(EventArgs e)
        {
            m_onMouse = false;
            base.OnMouseLeave(e);
        }

        /// <summary>
        /// override OnEnter
        /// </summary>
        /// <param name="e"></param>
        protected override void OnEnter(EventArgs e)
        {
            m_hasFocus = true;
            base.OnEnter(e);
        }

        /// <summary>
        /// override OnLeave
        /// </summary>
        /// <param name="e"></param>
        protected override void OnLeave(EventArgs e)
        {
            m_hasFocus = false;
            base.OnLeave(e);
        }

        /// <summary>
        /// override OnResize
        /// </summary>
        /// <param name="e"></param>
        protected override void OnResize(EventArgs e)
        {
            ClearCachedImage();
            base.OnResize(e);
        }

        /// <summary>
        /// override OnInvalidated
        /// </summary>
        /// <param name="e"></param>
        protected override void OnInvalidated(InvalidateEventArgs e)
        {
            ClearCachedImage();
            base.OnInvalidated(e);
        }

        /// <summary>
        /// override OnParentBackColorChanged
        /// </summary>
        /// <param name="e"></param>
        protected override void OnParentBackColorChanged(EventArgs e)
        {
            Invalidate(false);
            base.OnParentBackColorChanged(e);
        }

        /// <summary>
        /// override OnParentChanged
        /// </summary>
        /// <param name="e"></param>
        protected override void OnParentChanged(EventArgs e)
        {
            Invalidate(false);
            base.OnParentChanged(e);
        }

        /// <summary>
        /// 描画イメージを破棄します。
        /// Destroy images on memory for painting
        /// </summary>
        private void ClearCachedImage()
        {
            if (m_paintedFace != null)
            {
                m_paintedFace.Dispose();
                m_paintedFace = null;
            }

            if (m_clickedFace != null)
            {
                m_clickedFace.Dispose();
                m_clickedFace = null;
            }

            if (m_mouseOverFace != null)
            {
                m_mouseOverFace.Dispose();
                m_mouseOverFace = null;
            }

            if (m_focusedFace != null)
            {
                m_focusedFace.Dispose();
                m_focusedFace = null;
            }
        }

        /// <summary>
        /// ボタンの表明イメージを作成します。
        /// Create an image for a button face.
        /// </summary>
        /// <returns></returns>
        protected virtual Image CreateFaceImage(bool clicked)
        {
            var img = new Bitmap(this.ClientRectangle.Width, this.ClientRectangle.Height);

            var grp = GetGraphicsFromImage(img);

            //下地    fill with MatColor
            var fillColor = GetMatColor();
            var fillBrush = new SolidBrush(fillColor);
            grp.FillRectangle(fillBrush, new Rectangle(0, 0, img.Width, img.Height));
            fillBrush.Dispose();

            //ボタン内部
            //painting inner face

            Color backStartC;   //グラデーションの暗い色 dark color of gradation

            Color backColor;

            if (this.Enabled)
            {
                backColor = this.BackColor;
            }
            else
            {
                backColor = this.DisabledColor;
            }


            if (clicked)
            {   //クリック時のイメージは暗くします。
                //when button is clicked, dark color
                backStartC = ChangeBrightness(backColor, 0.92f);
            }
            else
            {                
                backStartC = backColor;
            }

            //グラデーションの明るい色
            //light color of gradation
            var backEndC = ChangeBrightness(backStartC, 1.55f);

            var backbrush = new LinearGradientBrush(this.ClientRectangle,
                                            backStartC,
                                            backEndC,
                                            LinearGradientMode.Vertical);

            backbrush.GammaCorrection = true;

            var fillpath = CreateControlShape(1, 1);

            grp.FillPath(backbrush, fillpath);

            fillpath.Dispose();            
            
            //テキストを描画します
            //draw text
            var sf = new StringFormat();

            sf.Alignment = StringAlignment.Center;
            sf.LineAlignment = StringAlignment.Center;
            sf.HotkeyPrefix = System.Drawing.Text.HotkeyPrefix.Show;    //アクセラレータキーは下線　show underline of an accelerator key charactor 

            var textRect = this.ClientRectangle;

            if (clicked)
            {
                textRect.Offset(0, 3);
                textRect.Inflate(0, -3);
            }
            else
            {
                textRect.Offset(0, 2);
                textRect.Inflate(0, -2);
            }


            if (this.Enabled)
            {
                var textBrush = new SolidBrush(this.ForeColor);
                grp.DrawString(this.Text, this.Font, textBrush, textRect, sf);
                textBrush.Dispose();                       
            }
            else
            {
                Color disabledForeColor = ChangeBrightness(this.DisabledColor, 2.0f);
                ControlPaint.DrawStringDisabled(grp, this.Text, this.Font, disabledForeColor, textRect, sf);
            }            

            //ハイライトの描画（白い楕円形を上部に描画）
            //draw hilight part (draw a white ellipse on the upper part)
            var hilightrect = new Rectangle(- (int)(img.Width * 0.4 / 2), 
                                            (int)(-img.Height / 2.0),
                                            (int)(img.Width * 1.4), img.Height);

            var hilightStartC = Color.FromArgb(150, Color.White);
            var hilightEndC = Color.FromArgb(45, Color.White);

            var hilightbrush = new LinearGradientBrush(hilightrect,
                                                         hilightStartC,
                                                         hilightEndC,
                                                         LinearGradientMode.Vertical);

            hilightbrush.GammaCorrection = true;

            grp.FillEllipse(hilightbrush, hilightrect);

            hilightbrush.Dispose();

            //境界線の描画
            //Draw boundary line
            var penColor = ChangeBrightness(backStartC, 0.7f);
            var drawPen = new Pen(penColor, 1);
            var drawpath = CreateControlShape(1, 1);
            grp.DrawPath(drawPen, drawpath);

            drawPen.Dispose();

            drawpath.Dispose();

            grp.Dispose();

            return img;
        }

        /// <summary>
        /// マウスオーバー時に表示するイメージを作成します。
        /// Create an image on mouseover.
        /// </summary>
        /// <returns></returns>
        protected virtual Image CreateMouseOverImage()
        {
            var img = new Bitmap(this.ClientRectangle.Width, this.ClientRectangle.Height);

            var grp = GetGraphicsFromImage(img);

            var path = CreateControlShape(1, 1);

            var br = new SolidBrush(Color.FromArgb(40, Color.White));

            grp.FillPath(br, path);

            br.Dispose();

            path.Dispose();
            
            grp.Dispose();

            return img;
        }

        /// <summary>
        /// フォーカスがあたっている場合に表示するイメージを作成します。
        /// Carete an image on having a focus.
        /// </summary>
        /// <returns></returns>
        protected virtual Image CreateFocusedImage()
        {
            var img = new Bitmap(this.ClientRectangle.Width, this.ClientRectangle.Height);

            var grp = GetGraphicsFromImage(img);
                       
            var path = CreateControlShape(0, 0);
            
            var drawPen1 = new Pen(Color.FromArgb(70, Color.White), 6);
            var drawPen2 = new Pen(Color.FromArgb(150, Color.White), 3);

            grp.DrawPath(drawPen1, path);
            grp.DrawPath(drawPen2, path);

            drawPen1.Dispose();
            drawPen2.Dispose();

            path.Dispose();

            grp.Dispose();

            return img;
        }

        private Graphics GetGraphicsFromImage(Image img)
        {
            var grp = Graphics.FromImage(img);

            grp.SmoothingMode = SmoothingMode.HighQuality;
            grp.InterpolationMode = InterpolationMode.HighQualityBicubic;
            grp.PixelOffsetMode = PixelOffsetMode.HighQuality;

            return grp;
        }

        /// <summary>
        /// 色の明度を変更します。
        /// Change brightness of color
        /// </summary>
        /// <param name="source"></param>
        /// <param name="rate"></param>
        /// <returns></returns>
        protected Color ChangeBrightness(Color source, float rate)
        {
            float r = Math.Max((float)source.R, 1f) * rate;
            float g = Math.Max((float)source.G, 1f) * rate;
            float b = Math.Max((float)source.B, 1f) * rate;

            return Color.FromArgb(source.A,
                                  (int)Math.Min(r, 255f),
                                  (int)Math.Min(g, 255f),
                                  (int)Math.Min(b, 255f));
        }


        /// <summary>
        /// コントロールの形のGraphicsPathオブジェクトを作成します。
        /// Create GraphicsPath object of this control's shape
        /// </summary>
        /// <param name="offset"></param>
        /// <param name="boundaryLineSize"></param>
        /// <returns></returns>
        protected virtual GraphicsPath CreateControlShape(int offset, int boundaryLineSize)
        {            
            
            var rect = this.DisplayRectangle;   //コントロールの四角形 rect of control
            int rad = this.CornerRadius;        //角の半径 corner radius

            //オフセット分調整
            //add a offset value
            rect.X += offset;
            rect.Y += offset;
            rect.Width -= offset;
            rect.Height -= offset;

            rect.Width -= boundaryLineSize;
            rect.Height -= boundaryLineSize;

            if (rect.Width <= 0)
            {
                rect.Width = 1;
            }

            if (rect.Height <= 0)
            {
                rect.Height = 1;
            }

            int minRad = Math.Min(rect.Width, rect.Height) / 2;

            if (rad > minRad)   
            {
                //コントロールより角丸の方が大きくなるようなら調整
                //if control size is smaller than corner size
                rad = minRad;
            }

            if (rad <= 0)
            {
                //最小値1 min value 1
                rad = 1;    
            }

            //角丸四角形のpathを作成
            //create a path of rect with rounded corners
            var path = new GraphicsPath();

            path.StartFigure();

            // 左上角  Top left corner
            path.AddArc(rect.Left, rect.Top, rad * 2, rad * 2, 180, 90);
            // 上線  Top line
            path.AddLine(rect.Left + rad, rect.Top, rect.Right - rad, rect.Top);
            // 右上角 Top right corner
            path.AddArc(rect.Right - rad * 2, rect.Top, rad * 2, rad * 2, 270, 90);
            // 右線 Right line
            path.AddLine(rect.Right, rect.Top + rad, rect.Right, rect.Bottom - rad);
            // 右下角 Bottom right corner
            path.AddArc(rect.Right - rad * 2, rect.Bottom - rad * 2, rad * 2, rad * 2,  0, 90);
            // 下線 Bottom line
            path.AddLine(rect.Right - rad, rect.Bottom, rect.Left + rad, rect.Bottom);
            // 左下角 Bottom left
            path.AddArc(rect.Left, rect.Bottom - rad * 2, rad * 2, rad * 2, 90, 90);
            // 左線 Left line
            path.AddLine(rect.Left, rect.Bottom - rad, rect.Left, rect.Top + rad);
            
            path.CloseFigure();

            return path;        
        }

        /// <summary>
        /// 下地に使用する色を取得します。MatColorプロパティがEmptyの場合は、Parent.BackColorを返します。
        /// Get controls mat color. if MatColor Property is not setted(Color.Empty), returning Parent.BackColor
        /// </summary>
        /// <returns></returns>
        protected Color GetMatColor()
        {
            if (this.MatColor.IsEmpty)
            {
                if (this.Parent != null)
                {
                    return this.Parent.BackColor;
                }
                else
                {
                    return SystemColors.Control;
                }
            }
            else
            {
                return this.MatColor;
            }
        }       
    }
}
