Glossy color button control for .NET desktop applications (System.Windows.Forms .NET Framework 2.0 or above)

You can use colorful buttons in form designer of Visual Studio on adding a dll file.

License : MIT

.NET デスクトップアプリケーション用のグロッシーな色付きボタンです。(System.Windows.Forms用 .NET Framework 2.0以上で動作します)

dllを追加するとVisual Studioのフォームデザイナ上で使えます。

MITライセンスです。